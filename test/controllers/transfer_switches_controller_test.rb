require 'test_helper'

class TransferSwitchesControllerTest < ActionController::TestCase
  setup do
    @transfer_switch = transfer_switches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transfer_switches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transfer_switch" do
    assert_difference('TransferSwitch.count') do
      post :create, transfer_switch: { activeStatus: @transfer_switch.activeStatus, dealer: @transfer_switch.dealer, map: @transfer_switch.map, modelNumber: @transfer_switch.modelNumber, msrp: @transfer_switch.msrp, productDescription: @transfer_switch.productDescription }
    end

    assert_redirected_to transfer_switch_path(assigns(:transfer_switch))
  end

  test "should show transfer_switch" do
    get :show, id: @transfer_switch
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transfer_switch
    assert_response :success
  end

  test "should update transfer_switch" do
    patch :update, id: @transfer_switch, transfer_switch: { activeStatus: @transfer_switch.activeStatus, dealer: @transfer_switch.dealer, map: @transfer_switch.map, modelNumber: @transfer_switch.modelNumber, msrp: @transfer_switch.msrp, productDescription: @transfer_switch.productDescription }
    assert_redirected_to transfer_switch_path(assigns(:transfer_switch))
  end

  test "should destroy transfer_switch" do
    assert_difference('TransferSwitch.count', -1) do
      delete :destroy, id: @transfer_switch
    end

    assert_redirected_to transfer_switches_path
  end
end
