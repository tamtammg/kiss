require 'test_helper'

class MiscItemLinesControllerTest < ActionController::TestCase
  setup do
    @misc_item_line = misc_item_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:misc_item_lines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create misc_item_line" do
    assert_difference('MiscItemLine.count') do
      post :create, misc_item_line: { bidSheet_id: @misc_item_line.bidSheet_id, itemName: @misc_item_line.itemName, itemPrice: @misc_item_line.itemPrice }
    end

    assert_redirected_to misc_item_line_path(assigns(:misc_item_line))
  end

  test "should show misc_item_line" do
    get :show, id: @misc_item_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @misc_item_line
    assert_response :success
  end

  test "should update misc_item_line" do
    patch :update, id: @misc_item_line, misc_item_line: { bidSheet_id: @misc_item_line.bidSheet_id, itemName: @misc_item_line.itemName, itemPrice: @misc_item_line.itemPrice }
    assert_redirected_to misc_item_line_path(assigns(:misc_item_line))
  end

  test "should destroy misc_item_line" do
    assert_difference('MiscItemLine.count', -1) do
      delete :destroy, id: @misc_item_line
    end

    assert_redirected_to misc_item_lines_path
  end
end
