require 'test_helper'

class BidSheetsControllerTest < ActionController::TestCase
  setup do
    @bid_sheet = bid_sheets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bid_sheets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bid_sheet" do
    assert_difference('BidSheet.count') do
      post :create, bid_sheet: { completeStatus: @bid_sheet.completeStatus, concreteslab_id: @bid_sheet.concreteslab_id, customer_id: @bid_sheet.customer_id, date: @bid_sheet.date, description: @bid_sheet.description, employee_id: @bid_sheet.employee_id, genAtsTax: @bid_sheet.genAtsTax, generator_id: @bid_sheet.generator_id, jobNumber: @bid_sheet.jobNumber, laborCost: @bid_sheet.laborCost, laborTax: @bid_sheet.laborTax, markup: @bid_sheet.markup, taxExempt: @bid_sheet.taxExempt, transferswitch_id: @bid_sheet.transferswitch_id }
    end

    assert_redirected_to bid_sheet_path(assigns(:bid_sheet))
  end

  test "should show bid_sheet" do
    get :show, id: @bid_sheet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bid_sheet
    assert_response :success
  end

  test "should update bid_sheet" do
    patch :update, id: @bid_sheet, bid_sheet: { completeStatus: @bid_sheet.completeStatus, concreteslab_id: @bid_sheet.concreteslab_id, customer_id: @bid_sheet.customer_id, date: @bid_sheet.date, description: @bid_sheet.description, employee_id: @bid_sheet.employee_id, genAtsTax: @bid_sheet.genAtsTax, generator_id: @bid_sheet.generator_id, jobNumber: @bid_sheet.jobNumber, laborCost: @bid_sheet.laborCost, laborTax: @bid_sheet.laborTax, markup: @bid_sheet.markup, taxExempt: @bid_sheet.taxExempt, transferswitch_id: @bid_sheet.transferswitch_id }
    assert_redirected_to bid_sheet_path(assigns(:bid_sheet))
  end

  test "should destroy bid_sheet" do
    assert_difference('BidSheet.count', -1) do
      delete :destroy, id: @bid_sheet
    end

    assert_redirected_to bid_sheets_path
  end
end
