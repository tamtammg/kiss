require 'test_helper'

class ConcreteSlabsControllerTest < ActionController::TestCase
  setup do
    @concrete_slab = concrete_slabs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:concrete_slabs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create concrete_slab" do
    assert_difference('ConcreteSlab.count') do
      post :create, concrete_slab: { slabName: @concrete_slab.slabName, slabPrice: @concrete_slab.slabPrice }
    end

    assert_redirected_to concrete_slab_path(assigns(:concrete_slab))
  end

  test "should show concrete_slab" do
    get :show, id: @concrete_slab
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @concrete_slab
    assert_response :success
  end

  test "should update concrete_slab" do
    patch :update, id: @concrete_slab, concrete_slab: { slabName: @concrete_slab.slabName, slabPrice: @concrete_slab.slabPrice }
    assert_redirected_to concrete_slab_path(assigns(:concrete_slab))
  end

  test "should destroy concrete_slab" do
    assert_difference('ConcreteSlab.count', -1) do
      delete :destroy, id: @concrete_slab
    end

    assert_redirected_to concrete_slabs_path
  end
end
