# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

State.delete_all
File.open('db/State.txt') do |states|
  states.read.each_line do |data|
    name, abbreviation = data.chomp.split("|")
    State.create!(:name => name, :abbreviation => abbreviation)
  end
end

Accessory.delete_all
File.open('db/Accessory.txt') do |accessories|
  accessories.read.each_line do |data|
    productDescription, modelNumber, dealer, msrp = data.chomp.split("|")
    Accessory.create!(:productDescription => productDescription, :modelNumber => modelNumber, :dealer => dealer, :msrp => msrp)
  end
end

Customer.delete_all
File.open('db/Customer.txt') do |customers|
  customers.read.each_line do |data|
    firstName, lastName, streetAddress, streetAddress2, city, state_id, zipCode, phone, phone2, email = data.chomp.split("|")
    Customer.create!(:firstName => firstName, :lastName => lastName, :streetAddress => streetAddress,
                     :streetAddress2 => streetAddress2, :city => city, :state_id => state_id,
                     :zipCode => zipCode, :phone => phone, :phone2 => phone2, :email => email)
  end
end

BidSheet.delete_all
File.open('db/BidSheet.txt') do |bidSheets|
  bidSheets.read.each_line do |data|
    employee_id, customer_id, jobNumber, date, generator_id, transferswitch_id, concreteslab_id, laborCost, laborTax, genAtsTax, markup, description = data.chomp.split("|")
    BidSheet.create!(:employee_id => employee_id, :customer_id => customer_id, :jobNumber => jobNumber, :date => date,
                     :generator_id => generator_id, :transferswitch_id => transferswitch_id, :concreteslab_id => concreteslab_id,
                     :laborCost => laborCost, :laborTax => laborTax, :genAtsTax => genAtsTax, :markup => markup,
                     :description => description)
  end
end

AccessoryLine.delete_all
File.open('db/AccessoryLine.txt') do |accessoryLines|
  accessoryLines.read.each_line do |data|
    bidSheet_id, accessory_id = data.chomp.split("|")
    AccessoryLine.create!(:bidSheet_id => bidSheet_id, :accessory_id => accessory_id)
  end
end

ConcreteSlab.create(slabName: 'Kohler Pad 14/20RESA(L)', slabPrice: '175')
ConcreteSlab.create(slabName: 'Generac OJ5339 GENPAD 54"X31"X3"', slabPrice: '231.62')

Employee.create(firstName: 'Burt', lastName: 'Koenig')
Employee.create(firstName: 'Nancy', lastName: 'Koenig')
Employee.create(firstName: 'Darryl', lastName: 'Turner')
Employee.create(firstName: 'David', lastName: 'Elsner')
Employee.create(firstName: 'Rick', lastName: 'Hunt')
Employee.create(firstName: 'Julio', lastName: 'Garcia')
Employee.create(firstName: 'Jose', lastName: 'Aguilar')
Employee.create(firstName: 'Steve', lastName: 'Bugge')

Generator.create(productDescription: '6,000 watt DC generator Air cooled', modelNumber: '6VSG Natural Gas/LPG', phVolt: '48', dealer: 5090, msrp: 5609, map: 5318, voltage: '48', mountedBreaker: '', alternator: '')
Generator.create(productDescription: '6,000 watt DC generator Air cooled', modelNumber: '6VSG-QS1', phVolt: '48', dealer: 5090, msrp: 5595, map: 5318, voltage: '48', mountedBreaker: '', alternator: '2F4')
Generator.create(productDescription: '6,000 watt DC generator Air cooled', modelNumber: '6VSG-QS2', phVolt: '48', dealer: 4829, msrp: 5304, map: 5041, voltage: '48', mountedBreaker: '', alternator: '2F5')
Generator.create(productDescription: '6,000 watt DC generator Air cooled', modelNumber: '6VSG-QS3', phVolt: '36', dealer: 4900, msrp: 5383, map: 5115, voltage: '36', mountedBreaker: '', alternator: '2F5')
Generator.create(productDescription: '6,000 watt DC generator Air cooled', modelNumber: '6VSG-QS9', phVolt: '24', dealer: 5401, msrp: 5540, map: 5265, voltage: '24', mountedBreaker: '', alternator: '2F5')
Generator.create(productDescription: '8,500 watt generator-Air cooled', modelNumber: '8.5RES-QS8', phVolt: '', dealer: 3211, msrp: 3499, map: 3308, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '14,000 watt generator-Air cooled', modelNumber: '14RESA-QS2', phVolt: '', dealer: 2969, msrp: 3394, map: 3233, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '14,000 watt generator-Air cooled', modelNumber: '14RESAL-SA4', phVolt: '', dealer: 3362, msrp: 3796, map: 3699, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '14,000 watt generator-Air cooled', modelNumber: '14RESAL-SA2', phVolt: '', dealer: 3466, msrp: 3930, map: 3742, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '20,000 watt generator-Air cooled', modelNumber: '20RESA-QS2', phVolt: '', dealer: 3639, msrp: 4197, map: 3997, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '20,000 watt generator-Air cooled', modelNumber: '20RESB', phVolt: '', dealer: 3681, msrp: 4197, map: 3997, voltage: '120/240', mountedBreaker: '', alternator: '2F7')
Generator.create(productDescription: '20,000 watt generator-Air cooled', modelNumber: '20RESAL-SA7', phVolt: '', dealer: 4093, msrp: 4722, map: 4497, voltage: '120/240', mountedBreaker: '125 amp', alternator: '4P5')
Generator.create(productDescription: '24,000 watt generator - Liquid cooled', modelNumber: '24RCL-QS (aluminum)', phVolt: '1Ph/240V', dealer: 8960, msrp: 9838, map: '', voltage: '120/240', mountedBreaker: '125 amp', alternator: '4E5.0')
Generator.create(productDescription: '24,000 watt generator - Liquid cooled', modelNumber: '24RCL-QS1 (aluminum)', phVolt: '3Ph/208V', dealer: 9224, msrp: 10129, map: '', voltage: '120/408', mountedBreaker: '125 amp', alternator: '4D5.0')
Generator.create(productDescription: '24,000 watt generator - Liquid cooled', modelNumber: '24RCL-QS2 (aluminum)', phVolt: '3Ph/240V', dealer: 9147, msrp: 10043, map: '', voltage: '120/240', mountedBreaker: '125 amp', alternator: '4D5.0')
Generator.create(productDescription: '24,000 watt generator - Liquid cooled', modelNumber: '24RCL -QS3 (aluminum)', phVolt: '3Ph/480V', dealer: 9202, msrp: 10104, map: '', voltage: '277/480', mountedBreaker: '125 amp', alternator: '4D5.0')
Generator.create(productDescription: '30,000 watt generator-Liquid cooled', modelNumber: '30RESA-QS8', phVolt: '', dealer: 10010, msrp: 11009, map: 10437, voltage: '120/240', mountedBreaker: '125 amp', alternator: '4P5')
Generator.create(productDescription: '38,000 watt generator-Liquid cooled', modelNumber: '38RCL-QS (aluminum)', phVolt: '1PH 120/240', dealer: 11843, msrp: 12073, map: 12450, voltage: '1/120/240', mountedBreaker: '175 amp', alternator: '4Q7BX')
Generator.create(productDescription: '39,000 watt generator-Liquid cooled', modelNumber: '38RCL-QS1 (aluminum)', phVolt: '3PH 120/208', dealer: 11913, msrp: 13151, map: 12525, voltage: '3/120/208', mountedBreaker: '150 amp', alternator: '4P7BX')
Generator.create(productDescription: '39,000 watt generator-Liquid cooled', modelNumber: '38RCL-QS2 (aluminum)', phVolt: '3PH 120/240', dealer: 11878, msrp: 13112, map: 12487, voltage: '3/120/240', mountedBreaker: '125 amp', alternator: '4P7BX')
Generator.create(productDescription: '39,000 watt generator-Liquid cooled', modelNumber: '38RCL-QS3 (aluminum)', phVolt: '3PH 277/480', dealer: 116744, msrp: 12962, map: 12345, voltage: '3/120/240', mountedBreaker: '60 amp', alternator: '4P7BX')
Generator.create(productDescription: '48,000 watt generator Liquid cooled', modelNumber: '48RCL-QS4 (aluminum)', phVolt: '1/120/240', dealer: 13682, msrp: 15136, map: 14415, voltage: '1/120/240', mountedBreaker: '200 amp', alternator: '4Q10X')
Generator.create(productDescription: '48,000 watt generator Liquid cooled', modelNumber: '48RCL-QS5 (aluminum)', phVolt: '3/120/208', dealer: 13805, msrp: 15262, map: 14535, voltage: '3/120/208', mountedBreaker: '175 amp', alternator: '4Q10X')
Generator.create(productDescription: '48,000 watt generator Liquid cooled', modelNumber: '48RCL-QS6 (aluminum)', phVolt: '3/120/240', dealer: 13770, msrp: 15222, map: 14498, voltage: '3/120/240', mountedBreaker: '150 amp', alternator: '4T9X')
Generator.create(productDescription: '48,000 watt generator Liquid cooled', modelNumber: '48RCL-QS7 (aluminum)', phVolt: '3/277/480', dealer: 13643, msrp: 15081, map: 14362, voltage: '3/277/480', mountedBreaker: '80 amp', alternator: '4T9X')
Generator.create(productDescription: '60,000 watt generator-Liquid cooled', modelNumber: '60RCL (aluminum)- Single Phase', phVolt: '', dealer: 15757, msrp: 18098, map: 17157, voltage: '120/240', mountedBreaker: '250 amp', alternator: '4T13X')
Generator.create(productDescription: '60,000 watt generator-Liquid cooled', modelNumber: '60ERESB-QS 2 (galvanized)', phVolt: '', dealer: 16125, msrp: 18098, map: 17157, voltage: '120/240', mountedBreaker: '250 amp', alternator: '4T13X')
Generator.create(productDescription: '80,000 watt generator-Liquid cooled', modelNumber: '80ERESD-QS2 (galvanized)', phVolt: '', dealer: 22689, msrp: 25498, map: 24172, voltage: '120/240', mountedBreaker: '350 amp', alternator: '4T13X')
Generator.create(productDescription: '80,000 watt generator-Liquid cooled', modelNumber: '80ERESD-QS3 (aluminum)', phVolt: '', dealer: 24243, msrp: 27298, map: 25879, voltage: '120/240', mountedBreaker: '350 amp', alternator: '4T13X')
Generator.create(productDescription: '100,000 watt generator-Liquid cooled', modelNumber: '100ERESD-QS6 (galvanized)', phVolt: '', dealer: 24301, msrp: 27400, map: 26847, voltage: '120/240', mountedBreaker: '600 amp', alternator: '')
Generator.create(productDescription: '100,000 watt generator-Liquid cooled', modelNumber: '100ERESD-QS7 (aluminum)', phVolt: '', dealer: 25831, msrp: 29098, map: 27585, voltage: '120/240', mountedBreaker: '600 amp', alternator: '')
Generator.create(productDescription: '125,000 watt generator-Liquid cooled', modelNumber: '125ERESC-QS (galvanized)', phVolt: '', dealer: 28998, msrp: 32650, map: 32325, voltage: '120/240', mountedBreaker: '600 amp', alternator: '')
Generator.create(productDescription: '125,000 watt generator-Liquid cooled', modelNumber: '125ERESC-QS3 (aluminum)', phVolt: '', dealer: 30532, msrp: 33786, map: 33937, voltage: '120/240', mountedBreaker: '600 amp', alternator: '')
Generator.create(productDescription: '150,000 watt generator-Liquid cooled', modelNumber: '150ERESC QS2 (galvanized)', phVolt: '', dealer: 37297, msrp: 42129, map: 39938, voltage: '', mountedBreaker: '', alternator: '')
Generator.create(productDescription: '150,000 watt generator-Liquid cooled', modelNumber: '150ERESC-QS3 (aluminum)', phVolt: '', dealer: 38619, msrp: 43626, map: 41358, voltage: '', mountedBreaker: '', alternator: '')

TransferSwitch.create(productDescription: 'NEMA 1 Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNA-0100A', dealer: 508, msrp: 567, map: 540)
TransferSwitch.create(productDescription: 'NEMA 1 Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNA-0200A', dealer: 732, msrp: 816, map: 778)
TransferSwitch.create(productDescription: 'NEMA 1 Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNA-0400A', dealer: 1559, msrp: 1739, map: 1656)
TransferSwitch.create(productDescription: 'NEMA 3R Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNC-0100A', dealer: 563, msrp: 628, map: 598)
TransferSwitch.create(productDescription: 'NEMA 3R Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNC-0200A', dealer: 788, msrp: 879, map: 837)
TransferSwitch.create(productDescription: 'NEMA 3R Without Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNC-0400A', dealer: 1706, msrp: 1903, map: 1813)
TransferSwitch.create(productDescription: 'Service Entry Rated 200 Amp ATS', modelNumber: 'RDT-CFNC-0200ASE', dealer: 1077, msrp: 1201, map: 1144)
TransferSwitch.create(productDescription: 'Service Entry Rated 400 Amp ATS', modelNumber: 'RDT-CFNC-0400ASE', dealer: 2530, msrp: 2822, map: 2688)
TransferSwitch.create(productDescription: 'NEMA 1 With Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNA-0100B', dealer: 571, msrp: 637, map: 606)
TransferSwitch.create(productDescription: 'NEMA 1 With Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNA-0200B', dealer: 853, msrp: 952, map: 906)
TransferSwitch.create(productDescription: 'NEMA 3R With Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNC-0100B', dealer: 626, msrp: 698, map: 665)
TransferSwitch.create(productDescription: 'NEMA 3R With Load Center 2 POLE Transfer Switch', modelNumber: 'RDT-CFNC-0200B', dealer: 993, msrp: 1108, map: 1055)
TransferSwitch.create(productDescription: 'Residential Intelligent Switch-NEMA 1', modelNumber: 'RSB-GFNA-0200-50NQS', dealer: 1200, msrp: 1339, map: 1275)
TransferSwitch.create(productDescription: 'Residential Intelligent Switch-NEMA 3', modelNumber: 'RSB-GFNC-0200-0125-NQS', dealer: 1306, msrp: 1457, map: 1387)
TransferSwitch.create(productDescription: 'Residential 100 amp NEMA 3', modelNumber: 'RXT-JFNC-0100A', dealer: 276, msrp: 359, map: 342)
TransferSwitch.create(productDescription: 'Residential 200 amp NEMA 3', modelNumber: 'RXT-JFNC-0200A', dealer: 558, msrp: 616, map: 587)
TransferSwitch.create(productDescription: 'Residential 400 amp NEMA 3', modelNumber: 'RXT-JFNC-0400A', dealer: 1698, msrp: 1882, map: 1792)
TransferSwitch.create(productDescription: 'Residential 100 amp NEMA 3 w/Load Center', modelNumber: 'RXT-JFNC-0100B', dealer: 529, msrp: 591, map: 562)
TransferSwitch.create(productDescription: 'Residential 100 amp NEMA 3 Service Rated', modelNumber: 'RXT-JFNC-0100ASE', dealer: 411, msrp: 513, map: 489)
TransferSwitch.create(productDescription: 'Residential 200 amp NEMA 3 Service Rated', modelNumber: 'RXT- JFNC-0200ASE', dealer: 653, msrp: 719, map: 685)
TransferSwitch.create(productDescription: 'Residential 400 amp NEMA 3 Service Rated', modelNumber: 'RXT- JFNC-0400ASE', dealer: 2366, msrp: 2777, map: 2645)
TransferSwitch.create(productDescription: 'Residential 150 amp NEMA 3 Service Rated', modelNumber: 'RXT- JFNC-0150ASE', dealer: 653, msrp: 719, map: 685)
TransferSwitch.create(productDescription: 'Residential 300 amp NEMA 3 Service Rated', modelNumber: 'RXT- JFNC-0300ASE', dealer: 2366, msrp: 2777, map: 2645)