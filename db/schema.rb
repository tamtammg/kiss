# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150331205035) do

  create_table "accessories", force: true do |t|
    t.string   "productDescription"
    t.string   "modelNumber"
    t.decimal  "dealer"
    t.decimal  "msrp"
    t.boolean  "activeStatus"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "accessory_lines", force: true do |t|
    t.integer  "bidSheet_id"
    t.integer  "accessory_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bid_sheets", force: true do |t|
    t.integer  "employee_id"
    t.integer  "customer_id"
    t.string   "jobNumber"
    t.date     "date"
    t.integer  "generator_id"
    t.integer  "transferswitch_id"
    t.integer  "concreteslab_id"
    t.decimal  "laborCost"
    t.decimal  "laborTax"
    t.decimal  "genAtsTax"
    t.decimal  "markup"
    t.boolean  "taxExempt"
    t.string   "description"
    t.boolean  "completeStatus"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "concrete_slabs", force: true do |t|
    t.string   "slabName"
    t.decimal  "slabPrice"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.string   "streetAddress"
    t.string   "streetAddress2"
    t.string   "city"
    t.integer  "state_id"
    t.integer  "zipCode"
    t.integer  "phone"
    t.integer  "phone2"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "generators", force: true do |t|
    t.string   "productDescription"
    t.string   "modelNumber"
    t.string   "phVolt"
    t.decimal  "dealer"
    t.decimal  "msrp"
    t.decimal  "map"
    t.string   "voltage"
    t.string   "mountedBreaker"
    t.string   "alternator"
    t.boolean  "activeStatus"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "misc_item_lines", force: true do |t|
    t.integer  "bidSheet_id"
    t.string   "itemName"
    t.decimal  "itemPrice"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transfer_switches", force: true do |t|
    t.string   "productDescription"
    t.string   "modelNumber"
    t.decimal  "dealer"
    t.decimal  "msrp"
    t.decimal  "map"
    t.boolean  "activeStatus"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
