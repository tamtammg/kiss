class CreateBidSheets < ActiveRecord::Migration
  def change
    create_table :bid_sheets do |t|
      t.integer :employee_id
      t.integer :customer_id
      t.string :jobNumber
      t.date :date
      t.integer :generator_id
      t.integer :transferswitch_id
      t.integer :concreteslab_id
      t.decimal :laborCost
      t.decimal :laborTax
      t.decimal :genAtsTax
      t.decimal :markup
      t.boolean :taxExempt
      t.string :description
      t.boolean :completeStatus

      t.timestamps
    end
  end
end
