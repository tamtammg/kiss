class CreateTransferSwitches < ActiveRecord::Migration
  def change
    create_table :transfer_switches do |t|
      t.string :productDescription
      t.string :modelNumber
      t.decimal :dealer
      t.decimal :msrp
      t.decimal :map
      t.boolean :activeStatus

      t.timestamps
    end
  end
end
