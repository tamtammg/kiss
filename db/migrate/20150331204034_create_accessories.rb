class CreateAccessories < ActiveRecord::Migration
  def change
    create_table :accessories do |t|
      t.string :productDescription
      t.string :modelNumber
      t.decimal :dealer
      t.decimal :msrp
      t.boolean :activeStatus

      t.timestamps
    end
  end
end
