class CreateConcreteSlabs < ActiveRecord::Migration
  def change
    create_table :concrete_slabs do |t|
      t.string :slabName
      t.decimal :slabPrice

      t.timestamps
    end
  end
end
