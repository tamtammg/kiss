class CreateMiscItemLines < ActiveRecord::Migration
  def change
    create_table :misc_item_lines do |t|
      t.integer :bidSheet_id
      t.string :itemName
      t.decimal :itemPrice

      t.timestamps
    end
  end
end
