class CreateGenerators < ActiveRecord::Migration
  def change
    create_table :generators do |t|
      t.string :productDescription
      t.string :modelNumber
      t.string :phVolt
      t.decimal :dealer
      t.decimal :msrp
      t.decimal :map
      t.string :voltage
      t.string :mountedBreaker
      t.string :alternator
      t.boolean :activeStatus

      t.timestamps
    end
  end
end
