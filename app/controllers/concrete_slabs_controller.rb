class ConcreteSlabsController < ApplicationController
  before_action :set_concrete_slab, only: [:show, :edit, :update, :destroy]

  # GET /concrete_slabs
  # GET /concrete_slabs.json
  def index
    @concrete_slabs = ConcreteSlab.all
  end

  # GET /concrete_slabs/1
  # GET /concrete_slabs/1.json
  def show
  end

  # GET /concrete_slabs/new
  def new
    @concrete_slab = ConcreteSlab.new
  end

  # GET /concrete_slabs/1/edit
  def edit
  end

  # POST /concrete_slabs
  # POST /concrete_slabs.json
  def create
    @concrete_slab = ConcreteSlab.new(concrete_slab_params)

    respond_to do |format|
      if @concrete_slab.save
        format.html { redirect_to @concrete_slab, notice: 'Concrete slab was successfully created.' }
        format.json { render :show, status: :created, location: @concrete_slab }
      else
        format.html { render :new }
        format.json { render json: @concrete_slab.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /concrete_slabs/1
  # PATCH/PUT /concrete_slabs/1.json
  def update
    respond_to do |format|
      if @concrete_slab.update(concrete_slab_params)
        format.html { redirect_to @concrete_slab, notice: 'Concrete slab was successfully updated.' }
        format.json { render :show, status: :ok, location: @concrete_slab }
      else
        format.html { render :edit }
        format.json { render json: @concrete_slab.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /concrete_slabs/1
  # DELETE /concrete_slabs/1.json
  def destroy
    @concrete_slab.destroy
    respond_to do |format|
      format.html { redirect_to concrete_slabs_url, notice: 'Concrete slab was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_concrete_slab
      @concrete_slab = ConcreteSlab.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def concrete_slab_params
      params.require(:concrete_slab).permit(:slabName, :slabPrice)
    end
end
