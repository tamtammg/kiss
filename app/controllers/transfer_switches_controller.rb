class TransferSwitchesController < ApplicationController
  before_action :set_transfer_switch, only: [:show, :edit, :update, :destroy]

  # GET /transfer_switches
  # GET /transfer_switches.json
  def index
    @transfer_switches = TransferSwitch.all
  end

  # GET /transfer_switches/1
  # GET /transfer_switches/1.json
  def show
  end

  # GET /transfer_switches/new
  def new
    @transfer_switch = TransferSwitch.new
  end

  # GET /transfer_switches/1/edit
  def edit
  end

  # POST /transfer_switches
  # POST /transfer_switches.json
  def create
    @transfer_switch = TransferSwitch.new(transfer_switch_params)

    respond_to do |format|
      if @transfer_switch.save
        format.html { redirect_to @transfer_switch, notice: 'Transfer switch was successfully created.' }
        format.json { render :show, status: :created, location: @transfer_switch }
      else
        format.html { render :new }
        format.json { render json: @transfer_switch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transfer_switches/1
  # PATCH/PUT /transfer_switches/1.json
  def update
    respond_to do |format|
      if @transfer_switch.update(transfer_switch_params)
        format.html { redirect_to @transfer_switch, notice: 'Transfer switch was successfully updated.' }
        format.json { render :show, status: :ok, location: @transfer_switch }
      else
        format.html { render :edit }
        format.json { render json: @transfer_switch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transfer_switches/1
  # DELETE /transfer_switches/1.json
  def destroy
    @transfer_switch.destroy
    respond_to do |format|
      format.html { redirect_to transfer_switches_url, notice: 'Transfer switch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer_switch
      @transfer_switch = TransferSwitch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_switch_params
      params.require(:transfer_switch).permit(:productDescription, :modelNumber, :dealer, :msrp, :map, :activeStatus)
    end
end
