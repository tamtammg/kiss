class BidSheetsController < ApplicationController
  before_action :set_bid_sheet, only: [:show, :edit, :update, :destroy]

  # GET /bid_sheets
  # GET /bid_sheets.json
  def index
    @bid_sheets = BidSheet.all
  end



  # GET /bid_sheets/1
  # GET /bid_sheets/1.json
  def show
  end

  # GET /bid_sheets/new
  def new
    @bid_sheet = BidSheet.new
  end

  # GET /bid_sheets/1/edit
  def edit
  end

  # POST /bid_sheets
  # POST /bid_sheets.json
  def create
    @bid_sheet = BidSheet.new(bid_sheet_params)

    respond_to do |format|
      if @bid_sheet.save
        format.html { redirect_to @bid_sheet, notice: 'Bid sheet was successfully created.' }
        format.json { render :show, status: :created, location: @bid_sheet }
      else
        format.html { render :new }
        format.json { render json: @bid_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bid_sheets/1
  # PATCH/PUT /bid_sheets/1.json
  def update
    respond_to do |format|
      if @bid_sheet.update(bid_sheet_params)
        format.html { redirect_to @bid_sheet, notice: 'Bid sheet was successfully updated.' }
        format.json { render :show, status: :ok, location: @bid_sheet }
      else
        format.html { render :edit }
        format.json { render json: @bid_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bid_sheets/1
  # DELETE /bid_sheets/1.json
  def destroy
    @bid_sheet.destroy
    respond_to do |format|
      format.html { redirect_to bid_sheets_url, notice: 'Bid sheet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bid_sheet
      @bid_sheet = BidSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bid_sheet_params
      params.require(:bid_sheet).permit(:employee_id, :customer_id, :jobNumber, :date, :generator_id, :transferswitch_id, :concreteslab_id, :laborCost, :laborTax, :genAtsTax, :markup, :taxExempt, :description, :completeStatus)
    end
end
