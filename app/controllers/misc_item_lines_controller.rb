class MiscItemLinesController < ApplicationController
  before_action :set_misc_item_line, only: [:show, :edit, :update, :destroy]

  # GET /misc_item_lines
  # GET /misc_item_lines.json
  def index
    @misc_item_lines = MiscItemLine.all
  end

  # GET /misc_item_lines/1
  # GET /misc_item_lines/1.json
  def show
  end

  # GET /misc_item_lines/new
  def new
    @misc_item_line = MiscItemLine.new
  end

  # GET /misc_item_lines/1/edit
  def edit
  end

  # POST /misc_item_lines
  # POST /misc_item_lines.json
  def create
    @misc_item_line = MiscItemLine.new(misc_item_line_params)

    respond_to do |format|
      if @misc_item_line.save
        format.html { redirect_to @misc_item_line, notice: 'Misc item line was successfully created.' }
        format.json { render :show, status: :created, location: @misc_item_line }
      else
        format.html { render :new }
        format.json { render json: @misc_item_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /misc_item_lines/1
  # PATCH/PUT /misc_item_lines/1.json
  def update
    respond_to do |format|
      if @misc_item_line.update(misc_item_line_params)
        format.html { redirect_to @misc_item_line, notice: 'Misc item line was successfully updated.' }
        format.json { render :show, status: :ok, location: @misc_item_line }
      else
        format.html { render :edit }
        format.json { render json: @misc_item_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /misc_item_lines/1
  # DELETE /misc_item_lines/1.json
  def destroy
    @misc_item_line.destroy
    respond_to do |format|
      format.html { redirect_to misc_item_lines_url, notice: 'Misc item line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_misc_item_line
      @misc_item_line = MiscItemLine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def misc_item_line_params
      params.require(:misc_item_line).permit(:bidSheet_id, :itemName, :itemPrice)
    end
end
