class AccessoryLinesController < ApplicationController
  before_action :set_accessory_line, only: [:show, :edit, :update, :destroy]

  # GET /accessory_lines
  # GET /accessory_lines.json
  def index
    @accessory_lines = AccessoryLine.all
  end



  # GET /accessory_lines/1
  # GET /accessory_lines/1.json
  def show
  end

  # GET /accessory_lines/new
  def new
    @accessory_line = AccessoryLine.new
  end

  # GET /accessory_lines/1/edit
  def edit
  end

  # POST /accessory_lines
  # POST /accessory_lines.json
  def create
    @accessory_line = AccessoryLine.new(accessory_line_params)

    respond_to do |format|
      if @accessory_line.save
        format.html { redirect_to @accessory_line, notice: 'Accessory line was successfully created.' }
        format.json { render :show, status: :created, location: @accessory_line }
      else
        format.html { render :new }
        format.json { render json: @accessory_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accessory_lines/1
  # PATCH/PUT /accessory_lines/1.json
  def update
    respond_to do |format|
      if @accessory_line.update(accessory_line_params)
        format.html { redirect_to @accessory_line, notice: 'Accessory line was successfully updated.' }
        format.json { render :show, status: :ok, location: @accessory_line }
      else
        format.html { render :edit }
        format.json { render json: @accessory_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accessory_lines/1
  # DELETE /accessory_lines/1.json
  def destroy
    @accessory_line.destroy
    respond_to do |format|
      format.html { redirect_to accessory_lines_url, notice: 'Accessory line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accessory_line
      @accessory_line = AccessoryLine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accessory_line_params
      params.require(:accessory_line).permit(:bidSheet_id, :accessory_id)
    end
end
