class Customer < ActiveRecord::Base
  belongs_to :state
  has_many :bid_sheets

  def set_cusfullname
    cusfullname = firstName + ' ' + lastName
    return cusfullname
  end

  def set_cusaddress
    cusaddress = streetAddress + ' ' + streetAddress2 + ', ' + city + ', ' + state.abbreviation + '  ' + zipCode.to_s
    return cusaddress
  end
end
