class BidSheet < ActiveRecord::Base
  belongs_to :employee
  belongs_to :customer
  belongs_to :transfer_switch
  belongs_to :concrete_slab
  belongs_to :generator
  has_many :misc_item_lines
  has_many :accessory_lines
  has_many :accessories, through: :accessory_lines



  validates :jobNumber, uniqueness: { scope: :jobNumber,
                                      message: "%{value} has already been used." }

end
