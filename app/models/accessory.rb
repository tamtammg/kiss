class Accessory < ActiveRecord::Base
  has_many :accessory_lines
  has_many :bid_sheets, through: :accessory_lines
end
