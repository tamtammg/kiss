json.array!(@bid_sheets) do |bid_sheet|
  json.extract! bid_sheet, :id, :employee_id, :customer_id, :jobNumber, :date, :generator_id, :transferswitch_id, :concreteslab_id, :laborCost, :laborTax, :genAtsTax, :markup, :taxExempt, :description, :completeStatus
  json.url bid_sheet_url(bid_sheet, format: :json)
end
