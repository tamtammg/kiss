json.array!(@transfer_switches) do |transfer_switch|
  json.extract! transfer_switch, :id, :productDescription, :modelNumber, :dealer, :msrp, :map, :activeStatus
  json.url transfer_switch_url(transfer_switch, format: :json)
end
