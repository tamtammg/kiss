json.array!(@customers) do |customer|
  json.extract! customer, :id, :firstName, :lastName, :streetAddress, :streetAddress2, :city, :state_id, :zipCode, :phone, :phone2, :email
  json.url customer_url(customer, format: :json)
end
