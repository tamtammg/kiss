json.array!(@concrete_slabs) do |concrete_slab|
  json.extract! concrete_slab, :id, :slabName, :slabPrice
  json.url concrete_slab_url(concrete_slab, format: :json)
end
